---
Spike Report
---

Graphics Spike 4
================

Introduction
------------

This Spike was completed in 2017 and the folder structure and repo was
updated in 2018.

Using some of the tools we learned about in GP Spike 3, we can create
both dynamic and static outdoor scenarios.

Bitbucket Link: <https://bitbucket.org/bSalmon842/graphicsspike4>

Goals
-----

-   A large landscape scene a large amount (100+) of low-poly foliage
    (use online assets) and an Atmospheric Fog Actor. Use the Third
    Person Character blueprint. We will create several copies so that we
    can compare and contrast the different lighting mobilities. The
    lighting setups for each copy is slightly different:

    -   Static level:

        -   Static Sky Light, Static Directional Light, and a Sky Sphere

        -   Create several sub-levels which will be [Lighting
            Scenarios](https://answers.unrealengine.com/questions/512084/who-can-explain-of-lighting-scenario-feature.html)
            for Morning, Midday, Afternoon/Evening, and Night, and a way
            to toggle the active one in the Level Blueprint

    -   Stationary level:

        -   Stationary Sky Light, Stationary Directional Light, and a
            Sky Sphere

        -   Create several sub-levels which will be [Lighting
            Scenarios](https://answers.unrealengine.com/questions/512084/who-can-explain-of-lighting-scenario-feature.html)
            for Morning, Midday, Afternoon/Evening, and Night, and a way
            to toggle the active one in the Level Blueprint

    -   Moveable level:

        -   Movable Sky Light, Movable Directional Light, a Sky Sphere

        -   Have the level blueprint rotate the directional light and
            update all relevant actors to have the time of day change
            (\~1 day/night cycle per 2 minutes).

        -   Consider moving the fog for different parts of the day (just
            using a spline or sine wave).

        -   Use [Distance Field Ambient
            Occlusion](https://docs.unrealengine.com/latest/INT/Engine/Rendering/LightingAndShadows/DistanceFieldAmbientOcclusion/index.html)

Personnel
---------

  ------------------------- ------------------
  Primary -- Brock Salmon   Secondary -- N/A
  ------------------------- ------------------

Technologies, Tools, and Resources used
---------------------------------------

-   <https://forums.unrealengine.com/community/community-content-tools-and-tutorials/30694-free-foliage-starter-kit>

-	<https://docs.unrealengine.com/latest/INT/Engine/Rendering/LightingAndShadows/DistanceFieldAmbientOcclusion/index.html>

-	<https://answers.unrealengine.com/questions/512084/who-can-explain-of-lighting-scenario-feature.html>

Tasks undertaken
----------------

-   To setup the basic environment a forest was made with over 100
    pieces of foliage, a Directional Light, Sky Light and Sky Sphere.

-   Multiple Scenarios were made for Morning, Midday, Evening and Night,
    except for the Movable variant, which was only one level

-   After creating each level they were attached to a master level and
    converted to lighting scenarios so that the player can switch
    between scenarios using the level blueprint

-   The Movable Level Blueprint rotates the sun and refreshes the sky
    sphere.

What we found out
-----------------

In this Spike we learned how to bake multiple lighting scenarios into one
level as well as how the difference in Light Mobility affects
performance, with Static being the best for Frames per Second and
Stationary being slightly better looking but with a performance hit. The
Distance field allows for far terrain to have consistent lighting. This
can be used for changing states quickly between scene transitions (maybe
a time transition between entering and leaving a building).